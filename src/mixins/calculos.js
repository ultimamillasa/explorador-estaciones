// src/mixins/calculos.js
export default {
	data() {
		let radius = 8000;
		return {
			variables: [
			{text: 'Carga de Bateria',value:'batVolts'},
			{text: 'Temperatura del Aire',value:'Temp_Aire'},
			{text: 'Humedad Relativa',value:'H_relativa'},
			{text: 'Presión Atmosferica',value:'P_atm'},
			{text: 'Precipitación Total',value:'Precip_Total'},
			{text: 'Velocidad del Viento',value:'Vel_viento'},
			{text: 'Dirección del Viento',value:'Dir_viento'},
			{text: 'Irradiancia',value:'Irradiancia'},
			{text: 'Altura Nieve',value:'Alt_Nieve'},
			{text: 'Temperatura Suelo',value:'T_Suelo'},
			],
			mBatVoltSerie: [],
			mTempAireSerie: [],
			mHRelativaSerie: [],
			mPAtmSerie: [],
			mPrecipTotalSerie: [],
			mVelVientoSerie: [],
			mDirVientoSerie: [],
			mIrradianciaSerie: [],
			mAltNieveSerie: [],
			mTSueloSerie: [],
			mAltNieveDSerie: [],
			mLabelsDate: [],
			lastdate: '',
			statsRange: [
			{radius: radius*2, color:'#9B2FB2',Temp_Aire: [-30,-25],H_relativa: [0],P_atm:[500.55],Precip_Total:[0],Vel_Viento:[0],Dir_Viento:[0],Irradiancia:[0],Alt_Nieve:[0],T_Suelo:[0]},
			{radius: radius*2, color:'#663FB4',Temp_Aire: [-25,-20],H_relativa: [1,20],P_atm:[500.56,500.6],Precip_Total:[0,0.4],Vel_Viento:[0,15],Dir_Viento:[0,50],Irradiancia:[0,0.2],Alt_Nieve:[0,0.5],T_Suelo:[0,4]},
			{radius: radius*2, color:'#4055B2',Temp_Aire: [-20,-15],H_relativa: [21,40],P_atm:[500.61,500.65],Precip_Total:[0.2,0.4],Vel_Viento:[16,30],Dir_Viento:[101,200],Irradiancia:[0.3,0.5],Alt_Nieve:[0.6,1.1],T_Suelo:[5,9]},
			{radius: radius*2, color:'#587CF7',Temp_Aire: [-15,-10],H_relativa: [41,60],P_atm:[500.66,500.71],Precip_Total:[0.5,0.7],Vel_Viento:[31,45],Dir_Viento:[201,300],Irradiancia:[0.6,0.8],Alt_Nieve:[1.2,1.7],T_Suelo:[10,14]},
			{radius: radius*2, color:'#1DAAF0',Temp_Aire: [-5,0],H_relativa: [61,80],P_atm:[500.72,500.77],Precip_Total:[0.8,1],Vel_Viento:[46,60],Dir_Viento:[301,350],Irradiancia:[0.9,1.1],Alt_Nieve:[1.8,2.3],T_Suelo:[15,19]},
			{radius: radius*2, color:'#1EBDD4',Temp_Aire: [0,5],H_relativa: [81,100],P_atm:[500.78,500.83],Precip_Total:[1.1, 1,6],Vel_Viento:[61,75],Dir_Viento:[351,360],Irradiancia:[1.2,1.6],Alt_Nieve:[2.4,2.9],T_Suelo:[20,24]},
			{radius: radius*2, color:'#309F27',Temp_Aire: [5,10],H_relativa: [81,100],P_atm:[500.78,500.83],Precip_Total:[1.1, 1,6],Vel_Viento:[61,75],Dir_Viento:[351,360],Irradiancia:[1.2,1.6],Alt_Nieve:[2.4,2.9],T_Suelo:[20,24]},
			{radius: radius*2, color:'#8CC051',Temp_Aire: [10,15],H_relativa: [81,100],P_atm:[500.78,500.83],Precip_Total:[1.1, 1,6],Vel_Viento:[61,75],Dir_Viento:[351,360],Irradiancia:[1.2,1.6],Alt_Nieve:[2.4,2.9],T_Suelo:[20,24]},
			{radius: radius*2, color:'#FDC12F',Temp_Aire: [15,20],H_relativa: [81,100],P_atm:[500.78,500.83],Precip_Total:[1.1, 1,6],Vel_Viento:[61,75],Dir_Viento:[351,360],Irradiancia:[1.2,1.6],Alt_Nieve:[2.4,2.9],T_Suelo:[20,24]},
			]
		}
	},
	methods: {
		maxminVariable(data,variable,tipo){
			var minmax = 0
			var min = Number.POSITIVE_INFINITY;
			var max = Number.NEGATIVE_INFINITY;
			var acum = 0
			data.forEach(function(dato) {
				var cifra = dato[variable]
				if(tipo == 'Maximo'){
					if (cifra > max) max = cifra;
				}else if(tipo == 'Minimo'){
					if (cifra < min) min = cifra;
				}else if(tipo == 'Promedio'){
					acum = acum + cifra
				}else{
			//Acumulado
			acum = acum + cifra
		}
	})
			if(tipo == 'Maximo'){
				minmax = max
			}
			if(tipo == 'Minimo'){
				minmax = min
			}
			if(tipo == 'Promedio'){
				minmax = acum/data.length
			}
			if(tipo == 'Acumulado'){
				minmax = acum
			}
			return minmax
		},
		agruparPorDia(data){
			// this gives an object with dates as keys
			const groups = data.reduce((groups, game) => {
				const date = game.Fecha.split('T')[0];
				if (!groups[date]) {
					groups[date] = [];
				}
				groups[date].push(game);
				return groups;
			}, {});
			// Edit: to add it in the array format instead
			const groupArrays = Object.keys(groups).map((date) => {
				return {
					date,
					games: groups[date]
				};
			});	
			return groupArrays
		},
		agruparPorMes(reg){
			var groups = reg.reduce(function (groups, o) {
				var m = o.Fecha.split(('-'))[0]+"-"+o.Fecha.split(('-'))[1]
				if (!groups[m]) {
					groups[m] = [];
				}
				groups[m].push(o);
				
				return groups;
			}, {});
			const result = Object.keys(groups).sort().map((m) => {
				return {
					date: m,
					games: groups[m]
				};
			});	
			return result
		},
		formatDate3(date) {
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;

			return [year, month, day].join('-');
		},
		formatDate (aFecha){
			var fecha = aFecha.replace("Z","")
			fecha = fecha.replace("T"," ")
			var date = new Date(fecha)
			var dd = date.getDate(); 
			var mm = date.getMonth()+1;
			var yyyy = date.getFullYear()
			var hh = date.getHours()
			var mi = (date.getMinutes()<10?'0':'') + date.getMinutes()
			var ss = (date.getSeconds()<10?'0':'') + date.getSeconds()
			if(dd<10){dd='0'+dd} 
				if(mm<10){mm='0'+mm}
					return dd+'/'+mm+'/'+yyyy+" "+hh+":"+mi+":"+ss
			},
			calculateRadius(t,v){
				var idx = 0
				var circle =  {
					radius: 0,
					color: '#fff',
					fillColor: '#fff',
					fillOpacity: 0.8,
				}
				this.statsRange.forEach(function(r,i){
					if(r[v].length > 1){
						if(t >= r[v][0] && t <= r[v][1]){
							idx = i
						}
					}else{
						if(t <= r[v][0]){
							idx = i
						}
					}
				})
				circle.radius = this.statsRange[idx].radius
				circle.color = this.statsRange[idx].color
				circle.fillColor = this.statsRange[idx].color
				return circle
			},
			random_rgba() {
				var o = Math.round, r = Math.random, s = 255;
				return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
			},
			getdiffbetDates(date11,date22){
				const date1 = new Date(date11);
				const date2 = new Date(date22);
				const diffTime = Math.abs(date2 - date1);
				const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
				//console.log(diffTime + " milliseconds");
				//console.log(diffDays + " days");
				return diffDays
			},
			formatDate2 (date) {
				if (!date) return null

					const [year, month, day] = date.split('-')
				return `${day}/${month}/${year}`
			},
			maxminprom(data,tipo,frec) {
				var auxThis = this
				data.forEach(function(dia){
					if(frec == 'mes'){
						auxThis.mLabelsDate.push(dia.date)
					}else{
						auxThis.mLabelsDate.push(auxThis.formatDate(dia.date))
					}
					var maxBatVol = 0
					var maxTempAire = 0
					var maxHRelativa = 0
					var maxPAtm = 0
					var maxPrecipTotal = 0
					var maxVelViento = 0
					var maxDirViento = 0
					var maxIrrad = 0
					var maxAltNie = 0
					var maxTsuelo = 0
					var maxAltNieD = 0

						//Para promedio
						var countDia = 0
						dia.games.forEach(function(dato){
							if(tipo == 'Maximo'){
								if(dato.batVolts > maxBatVol){
									maxBatVol = dato.batVolts
								}
								if(dato.Temp_Aire > maxTempAire){
									maxTempAire = dato.Temp_Aire
								}
								if(dato.H_relativa > maxHRelativa){
									maxHRelativa = dato.H_relativa
								}
								if(dato.P_atm > maxPAtm){
									maxPAtm = dato.P_atm
								}
								if(dato.Precip_Total > maxPrecipTotal){
									maxPrecipTotal = dato.Precip_Total
								}
								if(dato.Vel_Viento > maxVelViento){
									maxVelViento = dato.Vel_Viento
								}
								if(dato.Dir_Viento >  maxDirViento){
									maxDirViento = dato.Dir_Viento
								}
								if(dato.Irradiancia >  maxIrrad){
									maxIrrad = dato.Irradiancia
								}
								if(dato.Alt_Nieve >  maxAltNie){
									maxAltNie = dato.Alt_Nieve
								}
								if(dato.T_Suelo >  maxTsuelo){
									maxTsuelo = dato.T_Suelo
								}
								if(dato.Alt_NieveD >  maxAltNieD){
									maxAltNieD = dato.Alt_NieveD
								}
							}else if(tipo == 'Minimo'){
								if(dato.batVolts < maxBatVol){
									maxBatVol = dato.batVolts
								}
								if(dato.Temp_Aire < maxTempAire){
									maxTempAire = dato.Temp_Aire
								}
								if(dato.H_relativa < maxHRelativa){
									maxHRelativa = dato.H_relativa
								}
								if(dato.P_atm < maxPAtm){
									maxPAtm = dato.P_atm
								}
								if(dato.Precip_Total < maxPrecipTotal){
									maxPrecipTotal = dato.Precip_Total
								}
								if(dato.Vel_Viento < maxVelViento){
									maxVelViento = dato.Vel_Viento
								}
								if(dato.Dir_Viento <  maxDirViento){
									maxDirViento = dato.Dir_Viento
								}
								if(dato.Irradiancia <  maxIrrad){
									maxIrrad = dato.Irradiancia
								}
								if(dato.Alt_Nieve <  maxAltNie){
									maxAltNie = dato.Alt_Nieve
								}
								if(dato.T_Suelo <  maxTsuelo){
									maxTsuelo = dato.T_Suelo
								}
								if(dato.Alt_NieveD <  maxAltNieD){
									maxAltNieD = dato.Alt_NieveD
								}
							}else if(tipo == 'Promedio'){
								//Promedio
								maxBatVol = maxBatVol + dato.batVolts
								maxTempAire = maxTempAire + dato.Temp_Aire
								maxHRelativa = maxHRelativa + dato.H_relativa
								maxPAtm = maxPAtm + dato.P_atm
								maxPrecipTotal = maxPrecipTotal + dato.Precip_Total
								maxVelViento = maxVelViento + dato.Vel_Viento
								maxDirViento = maxDirViento + dato.Dir_Viento
								maxIrrad = maxIrrad + dato.Irradiancia
								maxAltNie = maxAltNie + dato.Alt_Nieve
								maxTsuelo = maxTsuelo + dato.T_Suelo
								maxAltNieD = maxAltNieD + dato.Alt_NieveD
							}else{
								//Acumulado
								maxBatVol = maxBatVol + dato.batVolts
								maxTempAire = maxTempAire + dato.Temp_Aire
								maxHRelativa = maxHRelativa + dato.H_relativa
								maxPAtm = maxPAtm + dato.P_atm
								maxPrecipTotal = maxPrecipTotal + dato.Precip_Total
								maxVelViento = maxVelViento + dato.Vel_Viento
								maxDirViento = maxDirViento + dato.Dir_Viento
								maxIrrad = maxIrrad + dato.Irradiancia
								maxAltNie = maxAltNie + dato.Alt_Nieve
								maxTsuelo = maxTsuelo + dato.T_Suelo
								maxAltNieD = maxAltNieD + dato.Alt_NieveD
							}
							countDia = countDia + 1 
						})
						if(tipo == 'Promedio'){
							maxBatVol = maxBatVol/countDia
							maxTempAire = maxTempAire/countDia
							maxHRelativa = maxHRelativa/countDia
							maxPAtm = maxPAtm/countDia
							maxPrecipTotal = maxPrecipTotal/countDia
							maxVelViento = maxVelViento/countDia
							maxDirViento = maxDirViento/countDia
							maxIrrad = maxIrrad/countDia
							maxAltNie = maxAltNie/countDia
							maxTsuelo = maxTsuelo/countDia
							maxAltNieD = maxAltNieD/countDia
						}

						auxThis.mBatVoltSerie.push(maxBatVol)
						auxThis.mTempAireSerie.push(maxTempAire)
						auxThis.mHRelativaSerie.push(maxHRelativa)
						auxThis.mPAtmSerie.push(maxPAtm)
						auxThis.mPrecipTotalSerie.push(maxPrecipTotal)
						auxThis.mVelVientoSerie.push(maxVelViento)
						auxThis.mDirVientoSerie.push(maxDirViento)
						auxThis.mIrradianciaSerie.push(maxIrrad)
						auxThis.mAltNieveSerie.push(maxAltNie)
						auxThis.mTSueloSerie.push(maxTsuelo)
						auxThis.mAltNieveDSerie.push(maxAltNieD)
					})
return [
auxThis.mBatVoltSerie,
auxThis.mTempAireSerie,
auxThis.mHRelativaSerie,
auxThis.mPAtmSerie,
auxThis.mPrecipTotalSerie,
auxThis.mVelVientoSerie,
auxThis.mDirVientoSerie,
auxThis.mIrradianciaSerie,
auxThis.mAltNieveSerie,
auxThis.mTSueloSerie,
auxThis.mAltNieveDSerie,
auxThis.mLabelsDate
]
}
}
};
