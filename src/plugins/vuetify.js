import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        primary: '#2196F3',
        secondary: '#CBC0D3',
        accent: '#DEE2FF',
        error: '#C25666',
        info: '#2D45FF',
        success: '#21F265',
        warning: '#F2CD21'
      },
    },
  },
  icons: {
    iconfont: 'md',
  },
});
